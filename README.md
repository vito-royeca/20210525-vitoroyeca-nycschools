# 20210525-VitoRoyeca-NYCSchools 

This is Vito Royeca's solution to the [Coding Challenge: NYC Schools](NYC Schools Code Challenge.md).

20210525-VitoRoyeca-NYCSchools is an iOS app written in SwiftUI and Combine. 

## Features

* Uses MapKit for displaying school locations

* Uses a list view of school locations in another tab

* Searchable schools data

* Detailed view of schools

## Shortcomings

* Preview view in SwiftUI is unavailable because the project starts with a numeric character. This is an Xcode limitation.

* JSON data is available only in memory and is not persisted to CoreData. This is due to time limitation in working on this code challenge. However, I worked in a past code challenge that persisted JSON to CoreData [here](https://github.com/vito-royeca/NYCSchools).  

## Building

Cocoapods dependencies are already included in this repository. You just need to open the [Xcode workspace file](20210525-VitoRoyeca-NYCSchools.xcworkspace).

## Author

Vito Royeca
jovito.royeca@cognizant.com
 
