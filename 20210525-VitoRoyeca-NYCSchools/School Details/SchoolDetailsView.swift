//
//  SchoolDetailsView.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/26/21.
//

import SwiftUI

/**
    The School Details view is a list view that is divided into sections.
 */
struct SchoolDetailsView: View {
    var school: School
    var satResult: SATResult?
    
    var body: some View {
        List {
            // Header section
            Section() {
                VStack {
                    Text(school.schoolName)
                        .font(.system(.title2))
                    Spacer()
                    Text(school.cleanAddress)
                        .font(.system(.subheadline))
                        .foregroundColor(Color.gray)
                    Spacer()
                    Text(school.overviewParagraph ?? "")
                        .font(.system(.body))
                }
            }
            
            // SAT Scores section
            Section(header: Text("SAT Scores")) {
                HStack {
                    Text("Number of Takers")
                    Spacer()
                    Text("\(satResult?.numOfSatTestTakers ?? 0)")
                        .multilineTextAlignment(.trailing)
                }
                HStack {
                    Text("Reading Avg Score")
                    Spacer()
                    Text("\(satResult?.satCriticalReadingAvgScore ?? 0)")
                        .multilineTextAlignment(.trailing)
                }
                HStack {
                    Text("Mathematics Avg Score")
                    Spacer()
                    Text("\(satResult?.satMathAvgScore ?? 0)")
                        .multilineTextAlignment(.trailing)
                }
                HStack {
                    Text("Writing Avg Score")
                    Spacer()
                    Text("\(satResult?.satWritingAvgScore ?? 0)")
                        .multilineTextAlignment(.trailing)
                }
            }
            
            // Contact Information section
            Section(header: Text("Contact Information")) {
                HStack {
                    Image(systemName: "phone")
                    Text(school.phoneNumber ?? "")
                }
                HStack {
                    Image(systemName: "faxmachine")
                    Text(school.faxNumber ?? "-")
                }
                HStack {
                    Image(systemName: "envelope")
                    Text(school.schoolEmail ?? "-")
                }
                HStack {
                    Image(systemName: "globe")
                    Text(school.website ?? "-")
                }
            }
        }
            .navigationBarTitle("School Details")
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView(school: School(dbn: "",
                                         faxNumber: nil,
                                         latitude: 0,
                                         location: nil,
                                         longitude: 0,
                                         overviewParagraph: nil,
                                         phoneNumber: nil,
                                         schoolEmail: nil,
                                         schoolName: "",
                                         totalStudents: 0,
                                         website: nil))
    }
}
