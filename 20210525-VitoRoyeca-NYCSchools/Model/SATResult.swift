//
//  SATResult.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import Foundation

/**
    This struct maps the SAT Results JSON data. Note that not all SAT results have corresponding School data (via the dbn property),
     hence the properties would be zero.
 */
struct SATResult: Codable, Identifiable {
    public var id: String {
        get {
            return dbn
        }
    }
    
    // Properties mapped from JSON
    public var dbn: String
    public var numOfSatTestTakers: Int32
    public var satCriticalReadingAvgScore: Int32
    public var satMathAvgScore: Int32
    public var satWritingAvgScore: Int32
    public var schoolName: String
}

extension SATResult {
    // JSON data are returned as Strings even for numeric types, hende we need to manually convert them to appropriate types
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        dbn = try values.decodeIfPresent(String.self, forKey: .dbn) ?? ""
        numOfSatTestTakers = try Int32(values.decodeIfPresent(String.self, forKey: .numOfSatTestTakers) ?? "0") ?? 0
        satCriticalReadingAvgScore = try Int32(values.decodeIfPresent(String.self, forKey: .satCriticalReadingAvgScore) ?? "0") ?? 0
        satMathAvgScore = try Int32(values.decodeIfPresent(String.self, forKey: .satMathAvgScore) ?? "0") ?? 0
        satWritingAvgScore = try Int32(values.decodeIfPresent(String.self, forKey: .satWritingAvgScore) ?? "0") ?? 0
        schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName) ?? ""
    }
}
