//
//  School.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import Foundation

/**
    This struct maps the NYC Schools JSON data and some select properties.
 */
struct School: Codable, Identifiable {
    // dbn is the ID
    public var id: String {
        get {
            return dbn
        }
    }

    // Removes the coordinate in parenthesis
    public var cleanAddress: String {
        get {
            return (location ?? "-").replacingOccurrences(of: "\\s?\\([^)]*\\)", with: "", options: .regularExpression)
        }
    }
    
    // Sections in list views, e.g. #, A, B, C
    public var section: String {
        get {
            var firstChar = String(schoolName.prefix(1))
            
            if firstChar.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil {
                firstChar = "#"
            }
            return firstChar
        }
    }
    
    // Select properties mapped from JSON. The JSON data contains 400+ properties.
    // We only chosed what are useful to our app
    public var dbn: String
    public var faxNumber: String?
    public var latitude: Double
    public var location: String?
    public var longitude: Double
    public var overviewParagraph: String?
    public var phoneNumber: String?
    public var schoolEmail: String?
    public var schoolName: String
    public var totalStudents: Int32
    public var website: String?
}

extension School {
    // JSON data are returned as Strings even for numeric types, hende we need to manually convert them to appropriate types
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        dbn = try values.decodeIfPresent(String.self, forKey: .dbn) ?? ""
        faxNumber = try values.decodeIfPresent(String.self, forKey: .faxNumber)
        latitude = try Double(values.decodeIfPresent(String.self, forKey: .latitude) ?? "0") ?? 0
        location = try values.decodeIfPresent(String.self, forKey: .location)
        longitude = try Double(values.decodeIfPresent(String.self, forKey: .longitude) ?? "0") ?? 0
        overviewParagraph = try values.decodeIfPresent(String.self, forKey: .overviewParagraph)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        schoolEmail = try values.decodeIfPresent(String.self, forKey: .schoolEmail)
        schoolName = try values.decodeIfPresent(String.self, forKey: .schoolName) ?? ""
        totalStudents = try Int32(values.decodeIfPresent(String.self, forKey: .totalStudents) ?? "0") ?? 0
        website = try values.decodeIfPresent(String.self, forKey: .website)
    }
}
