//
//  SchoolsFetcher.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import Foundation

/**
    This struct is used for making sections in the list views.
 */
struct SchoolSection: Identifiable {
    var id = UUID()
    var section: String
    var schools: [School]
}


/**
    This class fetches JSON data from the API and organizes them into sections.
 */
class SchoolsFetcher: ObservableObject {
    // MARK: - The NYC Schools API
    let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
    
    // MARK: - Variables
    @Published var schools = [School]()
    @Published var sections = [SchoolSection]()
    @Published var isBusy = false
    private var origSchools = [School]()
    
    init() {
        fetch()
    }

    /**
     Fetches JSON data from the API.
     */
    func fetch() {
        self.isBusy = true
        
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            do {
                if let data = data {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    self.origSchools = try decoder.decode([School].self, from: data)
                    
                    DispatchQueue.main.async {
                        self.filter(query: nil)
                        self.isBusy = false
                    }
                } else {
                    print("No data")
                    self.isBusy = false
                }
            } catch {
                print(error)
                self.isBusy = false
            }
        }.resume()
    }
    
    /**
      Filters the schools via schoolName property using the query parameter.
     */
    func filter(query: String?) {
        schools = Array(origSchools)
        
        if let query = query {
            if query.count == 1 {
                schools = schools.filter { $0.schoolName.lowercased().hasPrefix(query.lowercased())}
            } else if query.count > 1 {
                schools = schools.filter { $0.schoolName.lowercased().contains(query.lowercased())}
            }
        }
        schools = schools.sorted(by: {
            $0.schoolName < $1.schoolName
        })
        
        self.createSections()
    }
    
    /**
     Creates the sections to be presented into list views, e.g. #, A, B, C...Z
     */
    func createSections() {
        var dict = [String: [School]]()
        
        for school in schools {
            var array = dict[school.section] ?? [School]()
            array.append(school)
            dict[school.section] = array
            
        }
        
        sections = [SchoolSection]()
        for key in dict.keys.sorted() {
            sections.append(SchoolSection(section: key, schools: dict[key] ?? [School]()))
        }
    }
}
