//
//  SATResultsFetcher.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import Foundation

/**
    This class fetches JSON data from the API and organizes them into sections.
 */
class SATResultsFetcher: ObservableObject {
    
    // MARK: - The SAT Results API
    let url = URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
    
    // MARK: - Variables
    @Published var satResults = [SATResult]()
    @Published var isBusy = false
    
    init() {
        fetch()
    }

    /**
     Fetches JSON data from the API.
     */
    func fetch() {
        self.isBusy = true
        
        URLSession.shared.dataTask(with: url) {(data, response, error) in
            do {
                if let data = data {
                    let decoder = JSONDecoder()
                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                    
                    let satResults = try decoder.decode([SATResult].self, from: data)
                    
                    DispatchQueue.main.async {
                        self.satResults = satResults
                        self.isBusy = false
                    }
                } else {
                    print("No data")
                    self.isBusy = false
                }
            } catch {
                print(error)
                self.isBusy = false
            }
        }.resume()
    }
}
