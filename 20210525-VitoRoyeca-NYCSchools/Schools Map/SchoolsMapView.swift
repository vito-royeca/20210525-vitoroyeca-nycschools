//
//  SchoolsMapView.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import SwiftUI
import MapKit

/**
    This view uses MapKit to display the NYC map.
 */
struct SchoolsMapView: View {
    // MARK: - VAriables
    @EnvironmentObject var schoolsFetcher: SchoolsFetcher
    @EnvironmentObject var satResultsFetcher: SATResultsFetcher
    
    // center map to Central Park, NY
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 40.785091, longitude: -73.968285),
                                                   span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
    
    /**
     The annotations are clickable and will segue into the details view
    */
    var body: some View {
        NavigationView {
            Map(coordinateRegion: $region, annotationItems: schoolsFetcher.schools) { school in
                MapAnnotation(coordinate: CLLocationCoordinate2D(latitude: school.latitude, longitude: school.longitude), content: {
                    NavigationLink(destination: SchoolDetailsView(school: school,
                                                                  satResult: satResultsFetcher.satResults.filter{ $0.id == school.id }.first)) {
                        Image(systemName: "mappin")
                            .accentColor(.red)
                    }
                })
            }
            .edgesIgnoringSafeArea(.all)
            .navigationBarTitle("NYC Schools")
        }
    }
}

// Preview is not available beauce the project name starts with digits
struct SchoolsMapView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsMapView()
    }
}
