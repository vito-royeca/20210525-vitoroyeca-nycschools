//
//  _0210525_VitoRoyeca_NYCSchoolsApp.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import SwiftUI

@main
struct _0210525_VitoRoyeca_NYCSchoolsApp: App {
    // MARK: - Variables
    let schoolsFetcher = SchoolsFetcher()
    let satResultsFetcher = SATResultsFetcher()
    
    var body: some Scene {
        WindowGroup {
            MainView()
                // we add our variables to the environment
                .environmentObject(schoolsFetcher)
                .environmentObject(satResultsFetcher)
        }
    }
}
