//
//  ActivityIndicatorView.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/26/21.
//

import SwiftUI

struct ActivityIndicatorView: UIViewRepresentable {
    @Binding var shouldAnimate: Bool
    
    func makeUIView(context: Context) -> UIActivityIndicatorView {
        return UIActivityIndicatorView()
    }

    func updateUIView(_ uiView: UIActivityIndicatorView,
                      context: Context) {
        if self.shouldAnimate {
            uiView.startAnimating()
            
        } else {
            uiView.stopAnimating()
        }
    }
}
