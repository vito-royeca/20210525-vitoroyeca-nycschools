//
//  MainView.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import SwiftUI

struct MainView: View {
    // MARK: - Variables enherited from the environment. They are made available to the subviews of this view.
    @EnvironmentObject var schoolsFetcher: SchoolsFetcher
    @EnvironmentObject var satResultsFetcher: SATResultsFetcher
    
    var body: some View {
        if !schoolsFetcher.isBusy && !satResultsFetcher.isBusy {
            TabView {
                SchoolsMapView()
                    .tabItem {
                        Image(systemName: "map")
                        Text("Map")
                    }
             
                SchoolsListView()
                    .tabItem {
                        Image(systemName: "list.bullet")
                        Text("List")
                    }
            }
        } else {
            // let us display an indicator while we are loading data from the API
            ActivityIndicatorView(shouldAnimate: $schoolsFetcher.isBusy)
        }
    }
}

// Preview is not available beauce the project name starts with digits
struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
