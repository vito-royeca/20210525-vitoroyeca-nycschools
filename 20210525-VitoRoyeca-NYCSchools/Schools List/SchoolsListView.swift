//
//  SchoolsListView.swift
//  20210525-VitoRoyeca-NYCSchools
//
//  Created by Vito Royeca on 5/25/21.
//

import SwiftUI

/**
    This view is an alternative list view to the map view.
 */
struct SchoolsListView: View {
    // MARK: - Variables
    @EnvironmentObject var schoolsFetcher: SchoolsFetcher
    @EnvironmentObject var satResultsFetcher: SATResultsFetcher
    @State var query: String?
    @State var scopeSelection: Int = 0
    @State private var showCancelButton: Bool = false
    
    /**
     We include sections in our list view as well as a search bar to filter the schools based on name.
     The worss are clickable and will segue into the details view.
     */
    var body: some View {
        SearchNavigation(query: $query,
                         scopeSelection: $scopeSelection,
                         delegate: self) {
            List {
                ForEach(schoolsFetcher.sections) { section in
                    Section(header: Text(section.section)) {
                        ForEach(section.schools) { school in
                            NavigationLink(destination: SchoolDetailsView(school: school,
                                                                          satResult: satResultsFetcher.satResults.filter{ $0.id == school.id }.first)) {
                                VStack(alignment: .leading) {
                                    Text(school.schoolName)
                                    Text(school.cleanAddress)
                                        .font(.system(.subheadline))
                                        .foregroundColor(Color.gray)
                                }
                            }
                        }
                    }
                }
            }
                .navigationBarTitle(Text("NYC Schools"))
        }
            .edgesIgnoringSafeArea(.top)
    }
}

// Preview is not available beauce the project name starts with digits
struct SchoolsListView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolsListView()
    }
}

// MARK: - SearchNavigationDelegate

extension SchoolsListView: SearchNavigationDelegate {
    var options: [SearchNavigationOptionKey : Any]? {
        return [
            .automaticallyShowsSearchBar: true,
            .obscuresBackgroundDuringPresentation: true,
            .hidesNavigationBarDuringPresentation: true,
            .hidesSearchBarWhenScrolling: false,
            .placeholder: "Search",
            .showsBookmarkButton: false,
         ]
    }
    
    func search() {
        self.schoolsFetcher.filter(query: self.query)
    }
    
    func scope() {
        search()
    }
    
    func cancel() {
        search()
    }
}
