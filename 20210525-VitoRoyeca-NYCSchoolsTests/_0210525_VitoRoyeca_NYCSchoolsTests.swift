//
//  _0210525_VitoRoyeca_NYCSchoolsTests.swift
//  20210525-VitoRoyeca-NYCSchoolsTests
//
//  Created by Vito Royeca on 5/25/21.
//

import XCTest
@testable import _0210525_VitoRoyeca_NYCSchools

class _0210525_VitoRoyeca_NYCSchoolsTests: XCTestCase {
    let schoolJSON = """
    {
        "dbn": "02M260",
        "faxNumber": "212-524-4365",
        "latitude": "-73.992727",
        "location": "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
        "longitude": "40.736526",
        "overviewParagraph": "Lorem ipsum",
        "phoneNumber": "212-524-4360",
        "schoolEmail": "admissions@theclintonschool.net",
        "schoolName": "Clinton School Writers & Artists, M.S. 260",
        "totalStudents": "376",
        "website": "www.theclintonschool.net"
    }
    """
    
    let satResultJSON = """
    {
        "dbn": "01M292",
        "school_name": "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
        "num_of_sat_test_takers": "29",
        "sat_critical_reading_avg_score": "355",
        "sat_math_avg_score": "404",
        "sat_writing_avg_score":"363"
    }
    """
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSATResultCodable() {
        let jsonData = satResultJSON.data(using: .utf8)!
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let satResult = try! decoder.decode(SATResult.self, from: jsonData)
        
        XCTAssert(satResult.id == satResult.dbn)
        XCTAssert(satResult.dbn == "01M292")
        XCTAssert(satResult.schoolName == "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        XCTAssert(satResult.numOfSatTestTakers == 29)
        XCTAssert(satResult.satCriticalReadingAvgScore == 355)
        XCTAssert(satResult.satMathAvgScore == 404)
        XCTAssert(satResult.satWritingAvgScore  == 363)
        
    }
    
    func testSchoolCodable() {
        let jsonData = schoolJSON.data(using: .utf8)!
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        let school = try! decoder.decode(School.self, from: jsonData)
        XCTAssert(school.id == school.dbn)
        XCTAssert(school.section == "C")
        XCTAssert(school.cleanAddress == "10 East 15th Street, Manhattan NY 10003")
        XCTAssert(school.dbn == "02M260")
        XCTAssert(school.faxNumber == "212-524-4365")
        XCTAssert(school.latitude == Double(-73.992727))
        XCTAssert(school.location == "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)")
        XCTAssert(school.longitude == Double(40.736526))
        XCTAssert(school.overviewParagraph == "Lorem ipsum")
        XCTAssert(school.phoneNumber == "212-524-4360")
        XCTAssert(school.schoolEmail == "admissions@theclintonschool.net")
        XCTAssert(school.schoolName == "Clinton School Writers & Artists, M.S. 260")
        XCTAssert(school.totalStudents == Int32(376))
        XCTAssert(school.website == "www.theclintonschool.net")
    }
    
}
